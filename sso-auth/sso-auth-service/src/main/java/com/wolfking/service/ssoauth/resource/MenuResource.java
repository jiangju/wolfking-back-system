package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Menu;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.MenuService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/menu")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/menu", description = "菜单的服务", produces = MediaType.APPLICATION_JSON)
public class MenuResource {

	@Autowired
	private MenuService menuService;

	@POST
	@ApiOperation(value = "菜单添加接口", httpMethod = "POST", notes = "菜单添加接口", response = Boolean.class)
	public Response addMenu(@RequestBody Menu menu) {
		return ResponseUtil.okResponse(menuService.add(menu));
	}

	@PUT
	@ApiOperation(value = "菜单修改接口", httpMethod = "PUT", notes = "菜单修改接口", response = Boolean.class)
	public Response updateMenu(@RequestBody Menu menu) {
		return ResponseUtil.okResponse(menuService.update(menu));
	}

	@GET
	@ApiOperation(value = "查询所有的菜单", httpMethod = "GET", notes = "查询所有的菜单", response = Menu.class, responseContainer = "List")
	public Response getAllMenu() {
		return ResponseUtil.okResponse(menuService.findAll());
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询菜单接口", httpMethod = "GET", notes = "根据ID查询菜单接口", response = Menu.class)
	public Response getMenu(@PathParam("id") @ApiParam(required = true, name = "id", value = "菜单ID") String id) {
		Menu menu = menuService.getById(id);
		return ResponseUtil.okResponse(menu);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除菜单接口", httpMethod = "DELETE", notes = "根据ID删除菜单接口", response = Boolean.class)
	public Response deleteMenu(@PathParam("id") @ApiParam(required = true, name = "id", value = "菜单ID") String id) {
		return ResponseUtil.okResponse(menuService.deleteById(id));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询菜单信息", httpMethod = "POST", notes = "分页查询菜单信息", response = Menu.class, responseContainer = "PageInfo")
	public Response pageMenu(@RequestBody Menu menu,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<Menu> pageInfo = menuService.pageVague(menu, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}
}
