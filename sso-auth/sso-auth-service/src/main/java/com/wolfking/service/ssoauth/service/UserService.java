package com.wolfking.service.ssoauth.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.UserMapper;

/**
 * 用户的服务
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 上午11:00:14
 * @copyright wolfking
 */
@Service
public class UserService extends BaseService<UserMapper, User> {

	/**
	 * 根据角色ID查询用户
	 * 
	 * @param roleId
	 *            角色ID
	 * @return 用户列表
	 */
	@Transactional(readOnly = true)
	public List<User> getUserByRoleId(String roleId) {
		return mapper.getUserByRoleId(roleId);
	}

	/**
	 * 获取用户所有的鉴权码
	 * 
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<String> getUserAllAuthCodes(String userId) {
		return mapper.getUserAllAuthCodes(userId);
	}

	/**
	 * 获取用户所有的菜单Id
	 * 
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<String> getUserMenuIds(String userId) {
		return mapper.getUserAllMenuIds(userId);
	}

}
