package com.wolfking.service.ssoauth.service;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.back.core.bean.Role;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.RoleMapper;
import com.wolfking.service.ssoauth.mapper.RoleMenuRelationMapper;
import com.wolfking.service.ssoauth.mapper.RoleOfficeRelationMapper;
import com.wolfking.service.ssoauth.mapper.UserRoleRelationMapper;
import com.wolfking.service.ssoauth.relation.RoleMenuRelation;
import com.wolfking.service.ssoauth.relation.RoleOfficeRelation;
import com.wolfking.service.ssoauth.relation.UserRoleRelation;

/**
 * 角色的service
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月13日 下午9:29:15
 * @copyright wolfking
 */
@Service
public class RoleService extends BaseService<RoleMapper, Role> {

	@Autowired
	private RoleMenuRelationMapper roleMenuRelationMapper;

	@Autowired
	private RoleOfficeRelationMapper roleOfficeRelationMapper;

	@Autowired
	private UserRoleRelationMapper userRoleRelationMapper;

	@Override
	@Transactional
	public boolean add(Role role) {
		if (StringUtils.isEmpty(role.getId()))
			role.setId(UUID.randomUUID().toString().replace("-", ""));
		mapper.insert(role);
		List<RoleMenuRelation> roleMenuRelations = RoleMenuRelation.assemblyRelation(role);
		for (RoleMenuRelation roleMenuRelation : roleMenuRelations)
			roleMenuRelationMapper.insert(roleMenuRelation);
		List<RoleOfficeRelation> roleOfficeRelations = RoleOfficeRelation.assemblyRelation(role);
		for (RoleOfficeRelation roleOfficeRelation : roleOfficeRelations)
			roleOfficeRelationMapper.insert(roleOfficeRelation);
		return true;
	}

	@Override
	@Transactional
	public boolean update(Role role) {
		RoleMenuRelation relationM = new RoleMenuRelation();
		relationM.setRoleId(role.getId());
		roleMenuRelationMapper.deleteAccuracy(relationM);
		RoleOfficeRelation relationO = new RoleOfficeRelation();
		relationO.setRoleId(role.getId());
		roleOfficeRelationMapper.deleteAccuracy(relationO);
		mapper.update(role);
		List<RoleMenuRelation> relations = RoleMenuRelation.assemblyRelation(role);
		for (RoleMenuRelation roleMenuRelation : relations)
			roleMenuRelationMapper.insert(roleMenuRelation);
		List<RoleOfficeRelation> roleOfficeRelations = RoleOfficeRelation.assemblyRelation(role);
		for (RoleOfficeRelation roleOfficeRelation : roleOfficeRelations)
			roleOfficeRelationMapper.insert(roleOfficeRelation);
		return true;
	}

	@Override
	@Transactional
	public boolean deleteById(Object id) {
		Role role = new Role();
		role.setId(String.valueOf(id));
		mapper.deleteById(role);
		RoleMenuRelation relationM = new RoleMenuRelation();
		relationM.setRoleId(role.getId());
		roleMenuRelationMapper.deleteAccuracy(relationM);
		RoleOfficeRelation relationO = new RoleOfficeRelation();
		relationO.setRoleId(role.getId());
		roleOfficeRelationMapper.deleteAccuracy(relationO);
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public Role getById(Object id) {
		Role role = new Role();
		role.setId(String.valueOf(id));
		role = mapper.getById(role);
		RoleMenuRelation relationRM = new RoleMenuRelation();
		relationRM.setRoleId(role.getId());
		List<RoleMenuRelation> listRM = roleMenuRelationMapper.seleteAccuracy(relationRM);
		String menuIds = "", officeIds = "";
		if (listRM != null)
			for (RoleMenuRelation roleMenuRelation : listRM)
				menuIds += "," + roleMenuRelation.getMenuId();
		if (menuIds.length() > 0)
			menuIds = menuIds.substring(1);
		role.setMenuIds(menuIds);

		RoleOfficeRelation relationRO = new RoleOfficeRelation();
		relationRO.setRoleId(role.getId());
		List<RoleOfficeRelation> listRO = roleOfficeRelationMapper.seleteAccuracy(relationRO);
		if (listRO != null)
			for (RoleOfficeRelation roleOfficeRelation : listRO)
				officeIds += "," + roleOfficeRelation.getOfficeId();
		if (officeIds.length() > 0)
			officeIds = officeIds.substring(1);
		role.setOfficeIds(officeIds);
		return role;
	}

	@Transactional
	public void outrole(String roleId, String userId) {
		UserRoleRelation userRoleRelation = new UserRoleRelation(userId, roleId);
		userRoleRelationMapper.deleteAccuracy(userRoleRelation);
	}

	@Transactional
	public void assignrole(String roleId, String userId) {
		UserRoleRelation userRoleRelation = new UserRoleRelation(userId, roleId);
		userRoleRelationMapper.deleteAccuracy(userRoleRelation);
		userRoleRelationMapper.insert(userRoleRelation);
	}

}
