package com.wolfking.service.ssoauth.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.util.ResponseUtil;
import com.wolfking.service.ssoauth.service.DictService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Component
@Path("/dict")
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/dict", description = "字典的服务", produces = MediaType.APPLICATION_JSON)
public class DictResource {

	@Autowired
	private DictService dictService;

	@POST
	@ApiOperation(value = "字典添加接口", httpMethod = "POST", notes = "字典添加接口", response = Boolean.class)
	public Response addDict(@RequestBody Dict dict) {
		return ResponseUtil.okResponse(dictService.add(dict));
	}

	@PUT
	@ApiOperation(value = "字典修改接口", httpMethod = "PUT", notes = "字典修改接口", response = Boolean.class)
	public Response updateDict(@RequestBody Dict dict) {
		return ResponseUtil.okResponse(dictService.update(dict));
	}

	@GET
	@ApiOperation(value = "查询所有的字典", httpMethod = "GET", notes = "查询所有的字典", response = Dict.class, responseContainer = "List")
	public Response getAllDict() {
		return ResponseUtil.okResponse(dictService.findAll());
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "根据ID查询字典接口", httpMethod = "GET", notes = "根据ID查询字典接口", response = Dict.class)
	public Response getDict(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		Dict dict = dictService.getById(id);
		return ResponseUtil.okResponse(dict);
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "根据ID删除字典接口", httpMethod = "DELETE", notes = "根据ID删除字典接口", response = Boolean.class)
	public Response deleteDict(@PathParam("id") @ApiParam(required = true, name = "id", value = "用户ID") String id) {
		return ResponseUtil.okResponse(dictService.deleteById(id));
	}

	@POST
	@Path("/page")
	@ApiOperation(value = "分页查询字典信息", httpMethod = "POST", notes = "分页查询字典信息", response = Dict.class, responseContainer = "PageInfo")
	public Response pageDict(@RequestBody Dict dict,
			@HeaderParam("pageNum") @ApiParam(name = "pageNum", value = "页码", defaultValue = "1") int pageNum,
			@HeaderParam("pageSize") @ApiParam(name = "pageSize", value = "每页大小", defaultValue = "10") int pageSize) {
		PageInfo<Dict> pageInfo = dictService.pageVague(dict, pageNum, pageSize);
		return ResponseUtil.okResponse(pageInfo);
	}

	@GET
	@Path("/getByType")
	@ApiOperation(value = "根据type查询字典值", httpMethod = "GET", notes = "根据type查询字典值", response = Dict.class, responseContainer = "List")
	public Response getByType(
			@QueryParam("type") @ApiParam(name = "type", value = "字典的类别", defaultValue = "") String type) {
		Dict dict = new Dict();
		dict.setType(type);
		return ResponseUtil.okResponse(dictService.seleteAccuracy(dict));
	}

	@GET
	@Path("/getAllType")
	@ApiOperation(value = "获取字典的所有的type", httpMethod = "GET", notes = "获取字典的所有的type", response = String.class, responseContainer = "List")
	public Response getAllType() {
		return ResponseUtil.okResponse(dictService.getAllDictType());
	}
}
