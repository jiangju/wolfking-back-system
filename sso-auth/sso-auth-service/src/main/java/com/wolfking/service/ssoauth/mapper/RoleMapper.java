package com.wolfking.service.ssoauth.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.wolfking.back.core.bean.Role;
import com.wolfking.back.core.mybatis.BaseMapper;

/**
 * 角色的mapper映射
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 上午10:56:28
 * @copyright wolfking
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
