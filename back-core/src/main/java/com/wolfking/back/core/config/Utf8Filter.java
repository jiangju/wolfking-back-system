/**
 * 
 */
package com.wolfking.back.core.config;

import javax.servlet.annotation.WebFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * web - UTF-8字符编码的filter
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午1:59:52
 * @版权 归wolfking所有
 */
@WebFilter(urlPatterns = "/*", filterName = "utf-8-filter")
public class Utf8Filter extends CharacterEncodingFilter {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public Utf8Filter() {
		logger.info("filter set the utf-8 encoding");
		super.setEncoding("UTF-8");
		logger.info("filter forceEncoding true");
		super.setForceEncoding(true);
	}
}
