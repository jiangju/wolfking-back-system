/**
 * 
 */
package com.wolfking.back.core.bean;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyTable;

/**
 * 区域实体
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日上午10:00:52
 * @版权 归wolfking所有
 */
@MyTable("sys_area")
public class Area extends ParentEntity {

	private static final long serialVersionUID = 6009346317465791269L;
	@MyColumn
	private String type; // 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）
	@MyColumn
	private String code; // 区域编码
	@MyColumn
	private String name; // 区域名称
	@MyColumn
	private Integer sort; // 排序

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            要设置的 type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            要设置的 code
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return sort
	 */
	public Integer getSort() {
		return sort;
	}

	/**
	 * @param sort
	 *            要设置的 sort
	 */
	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
