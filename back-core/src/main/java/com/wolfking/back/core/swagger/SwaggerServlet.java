package com.wolfking.back.core.swagger;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * swagger的servlet
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月18日下午2:38:41
 * @版权 归wolfking所有
 */
@WebServlet(value = { "/swagger" }, name = "swagger")
public class SwaggerServlet extends HttpServlet {
	
	private static final long serialVersionUID = -4214414563318360854L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fullUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		if (fullUrl.endsWith("/"))
			fullUrl = fullUrl.substring(0, fullUrl.length() - 1);
		resp.sendRedirect(fullUrl + "/index.html");
	}
}
